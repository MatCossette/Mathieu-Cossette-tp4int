Mathieu-Cossette-tp4int



2021-03-28 - Mathieu-Cossette-tp2anim

Voici les animations ajoutées au site dans le cadre du TP2 d'animation:

1) Sur la page ACCUEIL: 
    Ajout d'un petit carré rouge qui va d'un côté à l'autre de la page. Dans le carré, il y a une balle qui rebondit dans tous les sens.
    Ajout d'un "call to action" pour s'inscrire à une infolettre. Le formulaire apparaît à côté du bonton.

2) Sur chaque page: 
    Ajout d'une tête de robot animée en svg au dessus du footer.    
    Ajout d'un effet hover sur le menu et les boutons.

3) Sur les pages BLOG et RÉALISATIONS:
    Ajout d'une animation au défilement ("animate on scroll").

