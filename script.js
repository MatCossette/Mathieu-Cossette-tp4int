let btnForm = document.getElementById("infolettre");
let formInfolettre = document.getElementById("form-infolettre");

btnForm.addEventListener("click", show);

function show() {
    formInfolettre.style.display = "block";
    anime({
      targets: '.form-infolettre',
      rotate: '1turn',
      duration: 1500
    });
}

